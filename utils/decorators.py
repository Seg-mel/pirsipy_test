import json
import posixpath
import time
from functools import wraps

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import select_template


def render_to(tmpl_path, ext_tmpl='_base.html', admin_page=False, manage_page=False):
    """
    Decorate the Django view.

    Wrap view that return dict of variables, that should be used for
    rendering the template.
    Dict returned from view could contain special keys:
    * MIME_TYPE: mimetype of response
    * TEMPLATE: template that should be used instead one that was
                specified in decorator argument
    """

    def decorator(func):

        @wraps(func)
        def wrapper(request, *args, **kwargs):
            request.admin_page = admin_page
            request.manage_page = manage_page

            output = func(request, *args, **kwargs)

            if not isinstance(output, dict):
                return output

            if request.is_ajax():
                # May need to be changed to use json_encode() snippet
                # if ever use this with the Django lazy strings.
                return HttpResponse(json.dumps(output), 'application/json')

            if not request.admin_page and not request.manage_page:
                output['ext_template'] = posixpath.join('core', ext_tmpl)
            elif request.admin_page:
                output['ext_template'] = posixpath.join('admin', ext_tmpl)

            template = output.pop('TEMPLATE') if 'TEMPLATE' in output \
                                              else tmpl_path

            # Try to find template in edition's directory first.
            # If there is no such template, try to use common one.
            # Will raise TemplateDoesNotExist exception if none of
            # templates exists.
            compiled_template = select_template([template])
            template = compiled_template.name

            kwargs = {'mimetype': output.pop('MIME_TYPE')} \
                                      if 'MIME_TYPE' in output\
                                      else {}

            return render_to_response(template,
                                      output,
                                      context_instance=RequestContext(request),
                                      **kwargs)
        return wrapper
    return decorator


def render_to_ajax(func):
    """
    Decorate the Django view returning an AJAX-destined object.
    """
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        result = func(request, *args, **kwargs)
        # Does it return a raw value or maybe a ready-to-use Response object
        # (e.g. error redirect)?
        if isinstance(result, HttpResponse):
            res = result
        else:
            status = result.pop('status_code', 200)
            res = HttpResponse(json.dumps(result), 'application/json', status)
        return res
    return wrapper


class CachedProperty(object):
    """Decorator for read-only properties evaluated only once within TTL period.

    It can be used to created a cached property like this::

        import random

        # the class containing the property must be a new-style class
        class MyClass(object):
            # create property whose value is cached for ten minutes
            @cached_property(ttl=600)
            def randint(self):
                # will only be evaluated every 10 min. at maximum.
                return random.randint(0, 100)

    The value is cached  in the '_cache' attribute of the object instance that
    has the property getter method wrapped by this decorator. The '_cache'
    attribute value is a dictionary which has a key for every property of the
    object which is wrapped by this decorator. Each entry in the cache is
    created only when the property is accessed for the first time and is a
    two-element tuple with the last computed property value and the last time
    it was updated in seconds since the epoch.

    The default time-to-live (TTL) is 300 seconds (5 minutes). Set the TTL to
    zero for the cached value to never expire.

    To expire a cached property value manually just do::

        del instance._cache[<property name>]

    2011 Christopher Arndt, MIT License"""
    def __init__(self, ttl=300):
        self.ttl = ttl

    def __call__(self, fget, doc=None):
        self.fget = fget
        self.__doc__ = doc or fget.__doc__
        self.__name__ = fget.__name__
        self.__module__ = fget.__module__
        return self

    def __get__(self, inst, owner):
        now = time.time()
        try:
            value, last_update = inst._cache[self.__name__]
            if self.ttl > 0 and now - last_update > self.ttl:
                raise AttributeError
        except (KeyError, AttributeError):
            value = self.fget(inst)
            try:
                cache = inst._cache
            except AttributeError:
                cache = inst._cache = {}
            cache[self.__name__] = (value, now)
        return value

cached_property = CachedProperty


def on_exception(exc=Exception, default=None, callback=None):
    r"""The decorator around some function C{f},
    such as when function C{f} is called and if it raises exception defined
    as C{exc} (which may be a single exception or a sequence of exceptions),
    then, the exception is ignored, and the result of the function will be
    either the result of the call of C{callback} (if one is present),
    or the C{default} value.

    In general, it is a rough equivalent of C{.get()} dict method, but for a
    function call raising some exception rather than for a dictionary
    item lookup.

    >>> @on_exception(ZeroDivisionError, 'BAD')
    ... def div1(x, y):
    ...     return x / y

    >>> div1(42, 0)
    'BAD'

    >>> def div2(x, y):
    ...     return x // y

    >>> div2(5, 0)
    Traceback (most recent call last):
       ...
    ZeroDivisionError: integer division or modulo by zero

    >>> on_exception((ZeroDivisionError, ValueError), 'BAD')(div2)(5, 0)
    'BAD'

    >>> def cb(exc):
    ...    print('Oops: {!s}'.format(exc))
    >>> on_exception((ZeroDivisionError, ValueError),
    ...              'BAD2',
    ...              lambda e: 'Oops: {!s}'.format(e))(div2)(5, 0)
    'Oops: integer division or modulo by zero'

    @type callback: col.Callable, NoneType
    """

    def wrap(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except exc as e:
                return callback(e) if callable(callback) else default

        return wrapper

    return wrap
