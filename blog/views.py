# coding=utf-8

from django.contrib.auth.models import User
from django.core.paginator import Paginator

from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import redirect

from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, FormView, View
from blog.forms import PostForm
from blog.models import Post
from utils.decorators import render_to
from utils.funcs import generate_username

__author__ = 'meloman'


class Blog(View):
    def get(self, request):
        return redirect(reverse('blog:blog_page', kwargs={'page': 1, 'username': request.user.username}))


class BlogPage(TemplateView):
    template_name = 'blog/page.html'

    @method_decorator(render_to(template_name))
    def get(self, request, *args, **kwargs):
        try:
            user = User.objects.get(username=kwargs['username'])
        except:
            raise Http404
        else:
            posts = Post.objects.filter(author=user).order_by('created')
            paginator = Paginator(posts, 10)
            return {
                'page': paginator.page(kwargs.get('page', 1)),
                'username': user.username,
            }


class PostPage(TemplateView):
    template_name = 'blog/post.html'

    @method_decorator(render_to(template_name))
    def get(self, request, *args, **kwargs):
        try:
            user = User.objects.get(username=kwargs['username'])
        except:
            raise Http404
        else:
            blog_post = Post.objects.get(slug=kwargs['slug'])
            return {
                'blog_post': blog_post,
                'username': user.username,
            }


class PostAddPage(FormView):
    template_name = 'blog/post_add.html'
    form_class = PostForm

    def get_initial(self):
        self.success_url = reverse('blog:blog_page', kwargs={'page': 1, 'username': self.request.user.username})

    @method_decorator(render_to(template_name))
    def get(self, request, *args, **kwargs):
        return {
            'form': self.form_class,
            'username': self.request.user.username,
        }

    def form_valid(self, form):
        data = form.cleaned_data
        Post.objects.create(
            title=data['title'],
            slug=generate_username(full_name=data['title'], model=Post, property='slug'),
            short_body=data['short_body'],
            body=data['body'],
            author=self.request.user,
            image=data['image'],
        )
        return super(PostAddPage, self).form_valid(form)

