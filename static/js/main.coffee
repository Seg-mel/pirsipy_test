class Main
    @clearUrlBeforeAuth: ->
        if window.location.hash is "#_=_"
            window.location.hash = ""
            history.pushState "", document.title, window.location.pathname


class BlogPage
    @changeLanguage: ->
        $(".language-link").on "click", (event) ->
            event.preventDefault()
            $form = $(document.forms["changeLanguage"])
            $languageOption = $form.find("option")
            languageCode = $(@).next().val()
            $languageOption.val languageCode
            $form.submit()

    @google_start: ->
        po = document.createElement("script")
        po.type = "text/javascript"
        po.async = true
        po.src = "https://plus.google.com/js/client:plusone.js?onload=start"
        s = document.getElementsByTagName("script")[0]
        s.parentNode.insertBefore po, s

    @google_button: ->
        $('.google-plus').on 'click', (event) ->
            event.preventDefault()
            $('.of-google-plus').find('button').click()
            $("#google-plus").submit()


@googlePlusSignInCallback = (result) ->
    if result["error"]
        console.log "An error happened:", result["error"]
    else
        $("#code").attr "value", result["code"]
        $("#at").attr "value", result["access_token"]


$ ->
    Main.clearUrlBeforeAuth()
    BlogPage.changeLanguage()
    BlogPage.google_start()
    BlogPage.google_button()