# coding=utf-8

from django import forms
from django.utils.translation import ugettext_lazy as _l, ugettext as _

__author__ = 'meloman'


class PalindromeForm(forms.Form):
    """ Palindrome form """
    string = forms.CharField(
        # Translators: Форма палиндрома. Поле ввода предложения
        label=_l(u'palindrome_form_string'),
        required=True,
        max_length=100,
        initial=u'',
        widget=forms.TextInput(attrs={
            # Translators: Форма палиндрома. Поле ввода предложения. Плейсхолдер
            'placeholder': _l(u'palindrome_form_string_placeholder'),
        })
    )