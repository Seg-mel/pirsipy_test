# coding=utf-8

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin

from core import views as core_views
from blog import views as blog_views
from pirsipy_test import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Admin
    url(r'^admin/', include(admin.site.urls)),
    # Internationalization
    url(r'^i18n/', include('django.conf.urls.i18n')),
    # Home
    url(r'^$', core_views.HomePage.as_view(), name='home'),
    # App urls
    url(r'^auth/', include('accounts.urls', namespace='accounts')),
    url(r'^(?P<username>[-\w]+)/blog/', include('blog.urls', namespace='blog')),
    url(r'^palindrome/', include('palindrome.urls', namespace='palindrome')),
    # Social auth
    url(r'^sa/', include('social.apps.django_app.urls', namespace='social')),
    # Other urls
    url(r'^blog/?$', blog_views.Blog.as_view(), name='blog_home'),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)