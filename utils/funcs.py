import calendar
from datetime import timedelta
import string
from django.contrib.auth.models import User
from django.utils.timezone import datetime, now
import pytz
from uuid import uuid4

from django.core.exceptions import SuspiciousOperation
from django.utils.translation import ugettext_lazy as _l, ugettext as _
from unidecode import unidecode


MONTHS = (
    (1, _(u'January')),
    (2, _(u'February')),
    (3, _(u'March')),
    (4, _(u'April')),
    (5, _(u'May')),
    (6, _(u'June')),
    (7, _(u'July')),
    (8, _(u'August')),
    (9, _(u'September')),
    (10, _(u'October')),
    (11, _(u'November')),
    (12, _(u'December')),

)


def ut(date_time):
    return calendar.timegm(date_time.timetuple())


def dt(unix_time):
    return datetime.datetime.utcfromtimestamp(float(unix_time))


def naive_to_utc(date_time):
    return date_time.replace(tzinfo=pytz.UTC)


def auth_user_has_profile(user):
    try:
        return user.is_authenticated and user.userprofile is not None
    except AttributeError:
        return False


def gen_unique_uid(cls, field='uid', length=32, retries=20):
    count = retries
    while count > 0:
        uid = str(uuid4().hex)[0:length]
        try:
            cls.objects.get(**{field: uid})
        except cls.DoesNotExist:
            return uid
        else:
            count -= 1
    else:
        raise Exception


def skip_suspicious_operations(record):
    """A C{logging} filter to skip the C{SuspiciousOperation} exceptions."""
    if record.exc_info:
        exc_value = record.exc_info[1]
        if isinstance(exc_value, SuspiciousOperation):
            return False
    return True


def coalesce(*args):
    """
    Given some arguments, return the first of them which is not None,
    or None.

    The name and the concept is inspired by the SQL standard COALESCE function.

    >>> coalesce(None, 5)
    5
    >>> coalesce(None, '', None)
    ''
    >>> coalesce(None, None, None, None) # Returns None
    """
    for arg in args:
        if arg is not None:
            return arg
    return None


def generate_username(full_name, model, property):
    """
    Username generation
    """
    def get_alphabet_index(count):
        alphabet_list = list(string.ascii_lowercase)
        alphabet_list_count = len(alphabet_list)
        alphabet_index = ''
        while count > 0:
            quotient, remainder = divmod(count, alphabet_list_count)
            alphabet_index += alphabet_list[remainder - 1]
            count = quotient
        return alphabet_index

    slug_username = ''.join(unidecode(full_name).lower().split(' '))
    user_count = model.objects.filter(**{'{}__startswith'.format(property): slug_username}).count()
    return '{}{}'.format(slug_username, get_alphabet_index(user_count))


def date_creator(year=None, month=None, day=None, get=None):
    """
    Date creator
    :param year: int or str
    :param month: int or str
    :param day: int or str
    :param get: None or 1
    :return: datetime.date or None
    """
    get = 1 if get is not None else get
    day = 1 if day is None else day
    if year and month:
        year, month, day = int(year), int(month), int(day)
        new_date = datetime(year, month, day).date()
    else:
        new_date = datetime(1, 1, 1).date() if get == 1 else None
    return new_date


def exist_bool(value, else_value=None):
    """ Return __value__ or __else_value__ by __value__ """
    try:
        value = int(value)
    except:
        pass
    return value if bool(value) else else_value


def num_input_validator(value, else_value=None):
    """
    Validator for number input
    :param value: all types
    :param else_value: value, that this function will return if will exception
    :return: int or else_value
    """
    try:
        value = int(value)
    except:
        value = else_value
    return value


def months_calendar(start=None, end=None):
    """
    Months calendar
    :param start: datetime.datetime object
    :param end: datetime.datetime object
    :return: datetime list by months
    """
    if not start:
        start = datetime.now()
    if not end:
        end = datetime.now()
    months, datetime_list = [], []
    cursor = start
    year = start.year
    while cursor <= end:
        last_month = months[-1] if months else None
        if cursor.month != last_month:
            months.append(cursor.month)
        cursor += timedelta(weeks=1)
    for month in months:
        year += 1 if month == 1 else 0
        datetime_list.append(datetime(year, month, 1))
    return datetime_list