# coding=utf-8

from django import forms
from django.utils.translation import ugettext_lazy as _l, ugettext as _

__author__ = 'meloman'


class LoginForm(forms.Form):
    """ Login form """
    login = forms.CharField(
        # Translators: Форма логина. Поле логина
        label=_l(u'login_form_email'),
        required=True,
        max_length=100,
        initial=u'',
    )
    password = forms.CharField(
        widget=forms.PasswordInput(),
        # Translators: Форма логина. Поле пароля
        label=_l(u'login_form_password'),
        required=True,
        max_length=100,
        initial='',
    )