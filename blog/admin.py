# coding=utf-8

from django.contrib import admin
from blog.models import Post

__author__ = 'meloman'


class PostFormAdmin(admin.ModelAdmin):
    exclude = ('slug',)


admin.site.register(Post, PostFormAdmin)