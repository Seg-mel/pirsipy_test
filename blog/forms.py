# coding=utf-8

from django import forms
from blog.models import Post

__author__ = 'meloman'


class PostForm(forms.ModelForm):
    """ Post of blog form """
    class Meta:
        model = Post
        exclude = ('created', 'slug', 'author')
