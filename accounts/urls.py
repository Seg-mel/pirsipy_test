# coding=utf-8

from django.conf.urls import patterns, url
from accounts import views as accounts_views

__author__ = 'meloman'


urlpatterns = patterns('',
    # Auth urls
    url(r'^login/?$', accounts_views.Login.as_view(), name='login'),
    url(r'^logout/?$', accounts_views.Logout.as_view(), name='logout'),

    # AJAX urls
)