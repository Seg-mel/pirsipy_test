# coding=utf-8

import logging
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from utils.decorators import render_to

__author__ = 'meloman'
logger = logging.getLogger(__name__)


class HomePage(TemplateView):
    template_name = 'core/home.html'

    @method_decorator(render_to(template_name))
    def get(self, request, *args, **kwargs):
        return {}