# coding=utf-8

from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _l, ugettext as _
from django.db import models
from os import path
from pytils.translit import slugify

__author__ = 'meloman'


class Post(models.Model):
    """ Post of blog model """

    def upload_to_path(instance, filename):
        filename_list = filename.split('.')
        if len(filename_list) > 1:
            file_name = ''.join(filename_list[:-1])
            file_ext = filename_list[-1]
            slug_file_name = '.'.join([slugify(file_name), file_ext])
        else:
            file_name = ''.join(filename_list)
            slug_file_name = ''.join([slugify(file_name)])
        return path.join('blogs', instance.author.username, slug_file_name)

    # Translators: Модель поста. Поле заголовка
    title = models.CharField(_l(u'post_title'), max_length=50)
    # Translators: Модель поста. Поле слага
    slug = models.CharField(_l(u'post_slug'), max_length=50)
    # Translators: Модель поста. Поле короткого содержания
    short_body = models.CharField(_l(u'post_short_body'), max_length=200)
    # Translators: Модель поста. Поле содержания
    body = models.TextField(_l(u'post_body'))
    author = models.ForeignKey(User)
    # Translators: Модель поста. Поле даты создания
    created = models.DateField(_l(u'post_created'), auto_now_add=True)
    # Translators: Модель поста. Поле изображения
    image = models.ImageField(_l(u'post_image'), upload_to=upload_to_path)

    def __unicode__(self):
        return unicode(u'#{id} [{user}] {title}'.format(id=self.id, user=self.author.username, title=self.title))

    class Meta:
        ordering = ('author', 'id')

