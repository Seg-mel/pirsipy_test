# coding=utf-8

import hashlib
import logging
from django.contrib.auth import logout as auth_logout, login as auth_login, get_backends, authenticate, login
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, FormView
from social.backends.google import GooglePlusAuth
from accounts.forms import LoginForm
from pirsipy_test import settings
from utils.decorators import render_to

__author__ = 'meloman'
logger = logging.getLogger(__name__)


class Login(FormView):
    template_name = 'accounts/login.html'
    form_class = LoginForm
    success_url = '/blog/'

    @method_decorator(render_to(template_name))
    def get(self, request, *args, **kwargs):
        plus_scope = ' '.join(GooglePlusAuth.DEFAULT_SCOPE)
        return {
            'plus_scope': plus_scope,
            'plus_id': settings.SOCIAL_AUTH_GOOGLE_PLUS_KEY,
            'form': self.form_class,
        }

    def form_valid(self, form):
        data = form.cleaned_data
        user = authenticate(username=data['login'], password=data['password'])
        if user is not None:
            if user.is_active:
                login(self.request, user)
        else:
            return super(Login, self).form_invalid(form)
        return super(Login, self).form_valid(form)


class Logout(TemplateView):
    """ Logout user """

    def get(self, request, **kwargs):
        auth_logout(request)
        return redirect(reverse('accounts:login'))
