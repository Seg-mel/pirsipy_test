# coding=utf-8

from django.utils.decorators import method_decorator

from django.views.generic import TemplateView
from palindrome.forms import PalindromeForm
from utils.decorators import render_to

__author__ = 'meloman'


def _palindrome_test(string):
    return string.lower().strip() == string.lower().strip()[::-1]


class HomePage(TemplateView):
    template_name = 'palindrome/home.html'

    @method_decorator(render_to(template_name))
    def get(self, request, *args, **kwargs):
        return {'form': PalindromeForm()}


class ResultPage(TemplateView):
    template_name = 'palindrome/result.html'

    @method_decorator(render_to(template_name))
    def post(self, request, *args, **kwargs):
        form = PalindromeForm(request.POST)
        data = None
        if form.is_valid():
            data = form.cleaned_data
        return {'palindrome': _palindrome_test(data['string']) if data else False}