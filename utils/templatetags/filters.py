# coding=utf-8

import json

from django import template

register = template.Library()


@register.filter
def subtract(value, arg):
    """ Subtraction filter """
    return value - arg


@register.filter
def order_by(queryset, fields):
    """ Queryset ordering  """
    return queryset.order_by(*[field.strip() for field in fields.split(',')])


@register.filter
def to_int(value):
    """ Converting to integer """
    return int(value)


@register.filter
def newline_to_br(value):
    """ Replacing '\n' symbol to '<br/> tag """
    return value.replace('\n', '<br/>')


@register.filter
def file_name(value):
    """ Getting only a file name from the path """
    return value.split('/')[-1] if '/' in value else value


@register.filter
def get_range(value, start_step):
    """
    Getting range
    :param value: string or integer
    :param start_step: string "int" or "int, int(not 0)"
    :return: python range
    Example:
    {{ 10|get_range:"0, 1" }}
    """
    start, step = 0, 1
    start_step_list = str(start_step.replace(' ', '')).split(',')
    if len(start_step_list) == 1:
        start = start_step
    elif len(start_step_list) == 2:
        start, step = start_step_list
    return range(int(start), int(value), int(step))


@register.filter
def get_half_list(value, half):
    """ Get half list by half number """
    return value[len(value)/2 + len(value) % 2 if half == 2 else None:
                 len(value)/2 + len(value) % 2 if half == 1 else None]


@register.filter
def json_dumps(value):
    """
    :param value: python object
    :return: json object
    """
    return json.dumps(value)