# coding=utf-8

from django.conf.urls import patterns, include, url
from django.contrib import admin
from blog import views as blog_views

urlpatterns = patterns('',
    # Blog urls
    url(r'^page/(?P<page>\d+)/?$', blog_views.BlogPage.as_view(), name='blog_page'),
    url(r'^post/(?P<slug>.+)/?$', blog_views.PostPage.as_view(), name='blog_post'),
    url(r'^add/?$', blog_views.PostAddPage.as_view(), name='post_add'),

    # AJAX urls
)