# coding=utf-8

import os

__author__ = 'meloman'


BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    '/Library/Python/2.7/site-packages/django/contrib/admin/static',
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

SCSS_EXECUTABLE = '/usr/bin/scss'
COFFEESCRIPT_EXECUTABLE = '/usr/local/bin/coffee'