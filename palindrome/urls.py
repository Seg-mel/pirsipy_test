# coding=utf-8

from django.conf.urls import patterns, include, url
from palindrome import views as palindrome_views

urlpatterns = patterns('',
    # Blog urls
    url(r'^$', palindrome_views.HomePage.as_view(), name='home'),
    url(r'^result/?$', palindrome_views.ResultPage.as_view(), name='result'),

    # AJAX urls
)